(function () {

    var el = document.querySelector(".location");
    var button = document.querySelector('button.convert');
    //display loading text while loading information.
    el.innerHTML = `<div class="loading">Loading</div>`;

    //get location coords to query api
    getLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                queryAPI(position.coords.latitude, position.coords.longitude);
            }, error => el.innerHTML = error.message);
        } else {
            el.innerHTML = "Geolocation is not supported";
        }
    }

    queryAPI = (lat, long) => {
        var query = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&APPID=49b624ff27fe7b91b9a93341eac30cf0&units=metric`;

        //check if anything is stored in localstorage
        if (localStorage.length == 0 ) {
            fetch(query)
            .then(response => {
                return response.json();
            }).then(data => {
                //convert fetch data to string before saving it to local storage
                var dataString = JSON.stringify(data);
                localStorage.setItem("dataString", dataString);
                populateDOM(localStorage.dataString);
                addColdOrHotClass();
            });
        }

        //add dynamic markup
        populateDOM(localStorage.dataString);
        addColdOrHotClass();

        //toggle farenheit
        var temp = document.querySelector(".temp");
        button.addEventListener("click", () => {
            var data = JSON.parse(localStorage.dataString);

            if (temp.innerHTML == data.main.temp) {
                temp.innerHTML = convertToFarenheit(data.main.temp);
            } else {
                temp.innerHTML = data.main.temp;
            }
        });
    }

    populateDOM = (data) => {
        var data = JSON.parse(localStorage.dataString);
        button.classList.add("show");
        el.innerHTML = `
        <div class="row">
            <div class="col-12">
                <h4>Current Weather</h4>
            </div>
            <div class="col-6">
                <p class="temp">${data.main.temp}</p>
            </div>
            <div class="col-6">
                <p>${data.weather[0].main}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <p>${data.name},</p>
            </div>
            <div class="col-6">
                <p>${data.sys.country}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-6"><p>Humidity</p></div>
            <div class="col-6"><p>${data.main.humidity}%</p></div>
        </div>
        <div class="row">
            <div class="col-6"><p>Wind Speed</p></div>
            <div class="col-6"><p>${data.wind.speed}</p></div>
        </div>
        <div class="row">
            <div class="col-6""><p>Sunrise</p></div>
            <div class="col-6""><p>${sun(data.sys.sunrise)}</p></div>
        </div>
        </div>
        <div class="row">
            <div class="col-6""><p>Sunset</p></div>
            <div class="col-6""><p>${sun(data.sys.sunset)}</p></div>
        </div>
        `;
    }

    addColdOrHotClass = () => {
        var data = JSON.parse(localStorage.dataString);

        if (data.main.temp < 8) {
            el.classList.add('cold');
            document.querySelector(
              ".weather"
            ).innerHTML = `<img src="https://www.shareicon.net/data/128x128/2015/09/30/109578_snow_512x512.png" />`;
        } else {
            el.classList.add('hot');
            document.querySelector(
              ".weather"
            ).innerHTML = `<img src="https://www.shareicon.net/data/128x128/2016/09/15/829311_sun_512x512.png" />`;
        }

    }

    convertToFarenheit = cel => {
        return (cel * 9 / 5) + 32;
    }

    sun = epochTime => {
        var d = new Date(epochTime * 1000); // The 0 there is the key, which sets the date to the epoch
        var hours = d.getHours();
        var minutes = "0" + d.getMinutes();
        var seconds = "0" + d.getSeconds();
        var time = hours + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);

        return time;
    }

    getLocation();

})();
