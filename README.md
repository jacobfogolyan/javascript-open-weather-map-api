# Getting Started

This application assumes that Node.js, NPM ,Gulp and the latest browser versions are installed locally on your machiene.

1. cd to project folder
1. npm install
1. run gulp

## Limitations

My prefered testing and development environment uses Chrome, however on initial startup my first decision was to use Firefox as Chrome does support local cookies in a non server environment.

I did encounter issues with this, so to ensure the API call would work consistently i used an app with VS Code call Live Server. This allowed me to then develop using my more familiar environment Chrome.

To ensure consistant installation of this application it runs in a server environment i decided to use Node.js, Gulp and NPM.

## Technology Stack

- NPM
- Node.js
- Gulp.js
- Bootstrap 4
- SASS
- Vanilla Javascript ES6 +

> I am aware of the limitations ES6 plus can have on older browsers. However this project is a demonstration of my technical knowledge.

## Functionality

1. Gets your geolocation
2. If Browser supports navigator.geolocation and the user accepts to allow navigator.geolocation
3. Then check if data has been installed into localStorage.
4. If no information is stored query api then store data in Local Storage
5. If Information is already store avoid api fetch and use local storage data
6. Populating the DOM with Dynamic Markup when parseing JSON data from localStorage Data.
7. Determine if weather is cold or hot and and add that to the parent node.
8. Toggle between Celcius and Farenheit temprature
9. If its hot change p colour to red
10. if its cold change p colour to blue.

## Notes

This app is limited to storing the information only once after the API Call and recalling that information regardless if the API is updated. Reading the [documentation](https://openweathermap.org/current) of the Current Weather API, data is updated every 10 minutes.

If I were to add functionality it would be to store another cookie in the browser rather than localStorage and set the expiration date for 10mins into the future. To ensure the most recent weather information is populated. However this was not in the scope of the documentation.
